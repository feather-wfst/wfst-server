#!/bin/sh

set -x
go build -o FeatherWFST_TESTS gitlab.com/feather-wfst/wfst-server
./FeatherWFST_TESTS &
FWFST_PID=${!}
go test -count=1 ${@} gitlab.com/feather-wfst/wfst-server/tests
kill ${FWFST_PID}
set +x

