module gitlab.com/feather-wfst/wfst-server

go 1.14

require (
	github.com/beevik/etree v1.1.0
	github.com/terminalstatic/go-xsd-validate v0.1.4
	gitlab.com/feather-wfst/geopackagego v0.0.0-20200817033722-7a5fb389df6c
)
