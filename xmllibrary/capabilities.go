// Copyright 2020 Roma Hicks

// This file is part of FeatherWFST.

// FeatherWFST is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// FeatherWFST is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FeatherWFST.  If not, see <https://www.gnu.org/licenses/>.

package xmllibrary

import (
	//"gitlab.com/feather-wfst/geopackagego"
	"github.com/beevik/etree"
)

type WFSServerInterface interface {
	// Service Identification
	GetServerTitle() string
	GetServerAbstract() string
	GetServiceType() string
	GetServiceTypeVersion() string
	// Filter Capabilities
	GetGeometryOperands() []string
	GetSpatialOperators() []string
	GetComparisonOperators() []string
	GetIDOperands() []string
	// Operations Metadata
	GetOperationsMetadata() OperationsMetadata
}

type OperationsMetadata struct {
	Operations []OperationMetadata
	Parameters []NameValuePair
	Constraints []NameValuePair
	}

type NameValuePair struct {
	Name string
	Values []string
	}

type OperationMetadata struct {
	Name    string
	Methods []OperationMethod
	Parameters []NameValuePair
	Constraints []NameValuePair
}

type OperationMethod struct {
	Name string
	URL  string
}

// BuildWFSCapabilitiesTemplate builds the XML data structure to advertise
// FeatherWFST's capabilities.
func BuildWFSCapabilitiesString(fS WFSServerInterface) (string, error) {
	doc := etree.NewDocument()
	doc.CreateProcInst("xml", `version="1.0" encoding="UTF-8"`)

	wfsCapabilitiesDoc := doc.CreateElement("wfs:WFS_Capabilities")
	wfsCapabilitiesDoc.CreateAttr("xmlns:wfs", "http://www.opengis.net/wfs")
	wfsCapabilitiesDoc.CreateAttr("xmlns:ows", "http://www.opengis.net/ows")
	wfsCapabilitiesDoc.CreateAttr("xmlns:ogc", "http://www.opengis.net/ogc")
	wfsCapabilitiesDoc.CreateAttr("xmlns:gml", "http://www.opengis.net/gml")
	wfsCapabilitiesDoc.CreateAttr("xmlns:xlink", "http://www.w3.org/1999/xlink")
	wfsCapabilitiesDoc.CreateAttr("version", "1.1.0")
	//wfsCapabilitiesDoc.CreateAttr("updateSequence", "1")

	// Declare Service Identification
	serviceID := wfsCapabilitiesDoc.CreateElement("ows:ServiceIdentification")
	serviceTitle := serviceID.CreateElement("ows:Title")
	serviceTitle.SetText(fS.GetServerTitle())
	serviceAbstract := serviceID.CreateElement("ows:Abstract")
	serviceAbstract.SetText(fS.GetServerAbstract())
	serviceType := serviceID.CreateElement("ows:ServiceType")
	serviceType.SetText(fS.GetServiceType())
	serviceTypeVersion := serviceID.CreateElement("ows:ServiceTypeVersion")
	serviceTypeVersion.SetText(fS.GetServiceTypeVersion())

	// ServiceProvider Section is not used.

	// Declare Operations Metadata
	operationsMeta := wfsCapabilitiesDoc.CreateElement("ows:OperationsMetadata")
	meta := fS.GetOperationsMetadata()
	for _, v := range meta.Operations {
		operation := operationsMeta.CreateElement("ows:Operation")
		operation.CreateAttr("name", v.Name)
		dcp := operation.CreateElement("ows:DCP")
		protocol := dcp.CreateElement("ows:HTTP")
		for _, m := range v.Methods {
			method := protocol.CreateElement(m.Name)
			method.CreateAttr("xlink:href", m.URL)
		}
		addParametersConstraints(v.Parameters, operation, "ows:Parameter")
		addParametersConstraints(v.Constraints, operation, "ows:Constraint")
	}
	addParametersConstraints(meta.Parameters, operationsMeta, "ows:Parameter")
	addParametersConstraints(meta.Constraints, operationsMeta, "ows:Constraint")

	// Declare Filter Capabilities
	filters := wfsCapabilitiesDoc.CreateElement("ogc:Filter_Capabilities")
	// Declare spatial filters capabilities.
	spatialCapabilities := filters.CreateElement("ogc:Spatial_Capabilities")
	geometryO := spatialCapabilities.CreateElement("ogc:GeometryOperands")
	geometryOperandList := fS.GetGeometryOperands()
	for _, v := range geometryOperandList {
		e := geometryO.CreateElement("ogc:GeometryOperand")
		e.SetText(v)
	}
	spatialO := spatialCapabilities.CreateElement("ogc:SpatialOperators")
	spatialOperatorsList := fS.GetSpatialOperators()
	for _, v := range spatialOperatorsList {
		e := spatialO.CreateElement("ogc:SpatialOperator")
		e.CreateAttr("name", v)
	}
	// Declare scalar filter capabalities.
	scalarCapabilities := filters.CreateElement("ogc:Scalar_Capabilities")
	comparisonO := scalarCapabilities.CreateElement("ogc:ComparisonOperators")
	compareOperatorsList := fS.GetComparisonOperators()
	for _, v := range compareOperatorsList {
		e := comparisonO.CreateElement("ogc:ComparisonOperator")
		e.SetText(v)
	}
	// Declare ID filter capabilities.
	idFilters := filters.CreateElement("ogc:Id_Capabilities")
	idOperandsList := fS.GetIDOperands()
	for _, v := range idOperandsList {
		idFilters.CreateElement(v)
	}

	doc.IndentTabs()
	return doc.WriteToString()
}

func addParametersConstraints(nvp []NameValuePair, parent *etree.Element, elementType string) {
	for _, n := range nvp {
		element := parent.CreateElement(elementType)
		element.CreateAttr("name", n.Name)
		for _, v := range n.Values {
			value := element.CreateElement("ows:Value")
			value.CreateText(v)
		}
	}
}
