// Copyright 2020 Roma Hicks

// This file is part of FeatherWFST.

// FeatherWFST is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// FeatherWFST is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with FeatherWFST.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"flag"
	"gitlab.com/feather-wfst/geopackagego"
	"gitlab.com/feather-wfst/wfst-server/httpserver"
	"gitlab.com/feather-wfst/wfst-server/xmllibrary"
	"log"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"
	"time"
)

var gpkgPath string
var listenAddr string

func init() {
	// Check flags.
	flag.StringVar(&gpkgPath, "geopackage", "geography.gpkg", "Path to the GeoPackage used by the server.")
	flag.StringVar(&listenAddr, "listen", ":6060", "Address to listen for clients.")
}

func main() {
	flag.Parse()
	var err error
	path, _ := filepath.Abs(gpkgPath)

	// Define all the supported operands.
	geometryOperands := []string{
		"gml:Envelope",
		"gml:Point",
		"gml:LineString",
		"gml:Polygon",
		"gml:PolyhedralSurface",
		"gml:Tin",
	}
	spatialOperators := []string{
		"BBOX",
	}
	comparisonOperators := []string{
		"LessThan",
		"GreaterThan",
		"LessThanEqualTo",
		"GreaterThanEqualTo",
		"EqualTo",
		"NotEqualTo",
	}
	idOperands := []string{
		"ogc:FID",
	}

	// Define Operation Metadata
	operationsMetadata := xmllibrary.OperationsMetadata{
		[]xmllibrary.OperationMetadata{
			{"GetCapabilities",
				[]xmllibrary.OperationMethod{
					{"ows:Get", "http://127.0.0.1:6060/wfs?"},
					{"ows:Post", "http://127.0.0.1:6060/wfs"},
				},
				[]xmllibrary.NameValuePair{ // Parameters
					{"AcceptVersion", []string{"1.1.0"}},
					{"AcceptFormat", []string{"text/xml"}},
				},
				[]xmllibrary.NameValuePair{}, // Constraints
			},
			{"DescribeFeatureType",
				[]xmllibrary.OperationMethod{
					{"ows:Get", "http://127.0.0.1:6060/wfs?"},
					{"ows:Post", "http://127.0.0.1:6060/wfs"},
				},
				[]xmllibrary.NameValuePair{}, // Parameters
				[]xmllibrary.NameValuePair{}, // Constraints
			},
			{"GetFeature",
				[]xmllibrary.OperationMethod{
					{"ows:Get", "http://127.0.0.1:6060/wfs?"},
					{"ows:Post", "http://127.0.0.1:6060/wfs"},
				},
				[]xmllibrary.NameValuePair{}, // Parameters
				[]xmllibrary.NameValuePair{}, // Constraints
			},
		},
		[]xmllibrary.NameValuePair{}, // Parameters
		[]xmllibrary.NameValuePair{ // Constraints
			{"DefaultMaxFeatures", []string{"100"}},
			{"DefaultLockExpiry", []string{"5"}},
			{"SupportsSOAP", []string{"True"}},
		},
	}

	// Intialise the WFS server structure.
	fS := FeatherServer{
		databasePath:        path,
		startTime:           time.Now(),
		ServerTitle:         "FeatherWFST",
		ServerAbstract:      "A simple WFS-T server that transmits features from a GeoPackage.",
		ServiceType:         "WFS",
		ServiceTypeVersion:  "1.1.0",
		GeometryOperands:    geometryOperands,
		SpatialOperators:    spatialOperators,
		ComparisonOperators: comparisonOperators,
		IDOperands:          idOperands,
		Operations:          operationsMetadata}
	log.Printf("SERVER: Starting server.\n")

	// Pass the WFS server to XML/HTTP server.
	fS.httpServer = httpserver.NewXMLServer(&fS)
	defer fS.httpServer.Close()

	// Open and load the geopackage.
	fS.database, err = geopackage.OpenGeoPackage(fS.databasePath)
	if err != nil {
		panic(err)
	}
	defer fS.database.DB.Close()

	fS.database.LoadContent()
	if err != nil {
		panic(err)
	}

	// Start listening for requests.
	fS.httpServer.Addr = listenAddr
	go func() {
		log.Printf("SERVER: Listening on %v\n", fS.httpServer.Addr)
		err = fS.httpServer.ListenAndServe()
		if err != nil {
			panic(err)
		}
	}()

	// Listen for shutdown signals to cleanly shutdown the server.
	signalChannel := make(chan os.Signal, 3)
	signal.Notify(signalChannel, syscall.SIGTERM, syscall.SIGINT)
	for {
		sig := <-signalChannel
		if sig == syscall.SIGTERM || sig == syscall.SIGINT {
			break
		}
	}

	log.Printf("SERVER: Server stopped.\n")
}
